﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace IOTesty
{
    public class Calculator
    {
        private const int PrefixLength= 2;
        public double Calculate(string formula)
        {
            if (formula == "")
                return 0.0;

            double result;
            string[] delimiters =new[] { "," };
            if (TestForDelimitersAndCutFormula(ref formula, out delimiters))
                if (TryParseAndSum(formula, delimiters, out result))
                    return result;

            if (MyTryParse(formula, out result))
                return result;

            if (TryParseAndSum(formula, new[] { "," }, out result))
                return result;

            if (TryParseAndSum(formula, new[] { "\n" }, out result))
                return result;

            throw new NotImplementedException();
        }

        private bool TryParseAndSum(string formula, string[] delimiters, out double result)
        {
            var isSum = false;
            result = 0.0;
            var divided = formula.Split(delimiters, StringSplitOptions.None);
            double temp = 0.0;
            foreach (var part in divided)
                if (MyTryParse(part, out temp))
                {
                    result += temp;
                    isSum = true;
                }

            return isSum;
        }

        private bool MyTryParse(string formula, out double result)
        {
            var isDouble = double.TryParse(formula, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign,
                CultureInfo.InvariantCulture, out result);

            if (result < 0)
                throw new ArgumentException();

            if (result > 1000)
                result = 0;

            return isDouble;
        }


        private bool TestForDelimitersAndCutFormula(ref string formula, out string[] delimiters)
        {
            delimiters = null;

            if (formula.StartsWith("//") && formula.Length > PrefixLength)
            {
                if (formula[PrefixLength] == '[')
                {
                    if (TestForMultiDelimitersAndCutFormula(ref formula, out delimiters))
                        return true;
                }

                if (!char.IsDigit(formula[PrefixLength]))
                {
                    delimiters = new[] { formula[PrefixLength].ToString() };
                    formula = formula.Substring(PrefixLength + 1, formula.Length - PrefixLength - 1);
                    return true;
                }
            }

            return false;
        }

        private bool TestForMultiDelimitersAndCutFormula(ref string formula, out string[] delimiters)
        {
            var delimitersList = new List<string>();
            var currentPosition = PrefixLength;
            var delimiterStart = PrefixLength + 1;

            while (currentPosition < formula.Length && formula[currentPosition] == '[')
            {
                for (; currentPosition < formula.Length; currentPosition++)
                    if (formula[currentPosition] == ']')
                    {
                        delimitersList.Add(formula.Substring(delimiterStart, currentPosition - delimiterStart));
                        delimiterStart = currentPosition + 2;
                        currentPosition++;
                        break;
                    }
            }
            formula = formula.Substring(currentPosition, formula.Length - currentPosition);
            delimiters = delimitersList.ToArray();

            if (delimitersList.Count >= 1)
                return true;

            return false;
        }
    }
}
