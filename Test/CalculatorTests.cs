using Microsoft.VisualStudio.TestTools.UnitTesting;
using IOTesty;
using System;

namespace Test
{
    [TestClass]
    public class CalculatorTests
    {
        private readonly Calculator _calculator;

        public CalculatorTests()
        {
            _calculator = new Calculator();
        }

        [TestMethod]
        public void TestEmpty()
        {
            //Given
            var formula = "";
            //When
            var result = _calculator.Calculate(formula);
            //Then
            Assert.AreEqual(0.0, result);
        }

        [TestMethod]
        public void TestNumber1()
        {
            //Given
            var formula = "1";
            //When
            var result = _calculator.Calculate(formula);
            //Then
            Assert.AreEqual(1.0, result);
        }

        [TestMethod]
        public void TestNumber8()
        {
            //Given
            var formula = "8";
            //When
            var result = _calculator.Calculate(formula);
            //Then
            Assert.AreEqual(8.0, result);
        }

        [TestMethod]
        public void TestSum()
        {
            //Given
            var formula = "2,4";
            //When
            var result = _calculator.Calculate(formula);
            //Then
            Assert.AreEqual(6.0, result);
        }

        [TestMethod]
        public void TestSumNewLine()
        {
            //Given
            var formula = "2\n4";
            //When
            var result = _calculator.Calculate(formula);
            //Then
            Assert.AreEqual(6.0, result);
        }

        [TestMethod]
        public void TestSumManyArgsComa()
        {
            //Given
            var formula = "3,4,6";
            //When
            var result = _calculator.Calculate(formula);
            //Then
            Assert.AreEqual(13.0, result);
        }

        [TestMethod]
        public void TestSumManyArgsNewLine()
        {
            //Given
            var formula = "3\n4\n6";
            //When
            var result = _calculator.Calculate(formula);
            //Then
            Assert.AreEqual(13.0, result);
        }

        [TestMethod]
        public void TestNegativeException()
        {
            //Given
            var formula = "-1";
            //When
            Func<object> func = () => _calculator.Calculate(formula);
            //Then
            Assert.ThrowsException<ArgumentException>(func);
        }

        [TestMethod]
        public void TestNegativeExceptionMany()
        {
            //Given
            var formula = "2,3,-1";
            //When
            Func<object> func = () => _calculator.Calculate(formula);
            //Then
            Assert.ThrowsException<ArgumentException>(func);
        }

        [TestMethod]
        public void TestSingleDelimiter()
        {
            //Given
            var formula = "//(2(3(41";
            //When
            var result = _calculator.Calculate(formula);
            //Then
            Assert.AreEqual(46.0, result);
        }

        [TestMethod]
        public void TestMultiLenDelimiter()
        {
            //Given
            var formula = "//[?^]2?^3?^41";
            //When
            var result = _calculator.Calculate(formula);
            //Then
            Assert.AreEqual(46.0, result);
        }

        [TestMethod]
        public void TestMultiDelimiter()
        {
            //Given
            var formula = "//[?^][*]2?^3?^41*9";
            //When
            var result = _calculator.Calculate(formula);
            //Then
            Assert.AreEqual(55.0, result);
        }
    }
}
